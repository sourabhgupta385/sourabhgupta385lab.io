https://sourabhgupta385.gitlab.io/index.html

Home Page:
    Complete:
        - Layout complete
        - Products section filters working fine
        - Banner working fine (The Summer Collection one)
    
    Pending:
        - Need to replace logo (Needed)
        - Need to update caption in big pink banners (Needed)
        - Need to update product images (Will need low size images - See Sample Product Images folder in drive) (Needed)
        - Need to update caption in banners (The Summer Collection one) (Needed)
        - Need to update instagram section images (If required, Will need low size images - See Sample Insta Images folder in drive) (Needed)
        - Need to update social media links (Needed)

Shop Page: 
    Complete:
        - Layout complete
    
    Pending:
        - Filters are not working fine yet, Will work on them
        - Need to update product images (Will need low size images - See Sample Product Images folder in drive) (Needed)

About Page:
    Pending:
        - Need content of it
        - Need to decide and design layout of it

Contact Page: 
    Complete:
        - Layout is complete
    
    Pending: 
        - Need its data
        - I need to check on email, whether email can be triggered automatically from website
        - Need google maps location
