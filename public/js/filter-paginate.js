var itemsNumber = 6;
var min = 0;
var max = itemsNumber;

function pagination(action, products) {
    if ( products == undefined) {
      products = $(".product")
    }
    var totalItems = products.length;
    $("#next").show();
    $("#prev").show();

    //Stop action if max reaches more than total items
    if (max < totalItems) {
        if (action == "next") {
            min = min + itemsNumber;
            max = max + itemsNumber;
        }
    }

    //Stop action if min reaches less than 0
    if (min > 0) {
        if (action == "prev") {
            min = min - itemsNumber;
            max = max - itemsNumber;
        }
    }
    if (max >= totalItems) $("#next").hide();

    if (min <= 0) $("#prev").hide();

    products.hide();
    products.slice(min, max).show();
}

pagination();

//Next
$("#next").click(function() {
    action = "next";
    if ( $filteredResults == "" ) {
      pagination(action)  
    }
    else {
      pagination(action, $(".product").filter($filteredResults));
    }
})

//Previous
$("#prev").click(function() {
    action = "prev";
    if ( $filteredResults == "" ) {
      pagination(action)  
    }
    else {
      pagination(action, $(".product").filter($filteredResults));
    }
})

var $filterCheckboxes = $('input[type="checkbox"]');

// create a collection containing all of the filterable elements
var $filteredResults = ""

$filterCheckboxes.on("change", function () {
    var selectedFilters = {};

    $filterCheckboxes.filter(":checked").each(function () {
        if (!selectedFilters.hasOwnProperty(this.name)) {
            selectedFilters[this.name] = [];
        }
        selectedFilters[this.name].push(this.value);
    });

    // create a collection containing all of the filterable elements
    $filteredResults = $(".product");

    // loop over the selected filter name -> (array) values pairs
    $.each(selectedFilters, function (name, filterValues) {
        // filter each .product element
        $filteredResults = $filteredResults.filter(function () {
            var matched = false,
                currentFilterValues = $(this).data("category").split(" ");

            // loop over each category value in the current .product's data-category
            $.each(currentFilterValues, function (_, currentFilterValue) {
                // if the current category exists in the selected filters array
                // set matched to true, and stop looping. as we're ORing in each
                // set of filters, we only need to match once

                if ($.inArray(currentFilterValue, filterValues) != -1) {
                    matched = true;
                    return false;
                }
            });

            // if matched is true the current .product element is returned
            return matched;
        });
    });

    // $(".product").hide().filter($filteredResults).show();
    $(".product").hide()
    pagination("", $(".product").filter($filteredResults))
});
